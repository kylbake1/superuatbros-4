﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public static bool GameIsPaused; // a bool is created to see if game is paused

    public GameObject pauseMenuUI; // allows us to insert the pause menu in inspector

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P)) // gets user input for pressing the P key
        {
            if (GameIsPaused) // if the bool is false
            {
                Resume(); // resume the game
            }
            else // if its true
            {
                Pause(); // pause the game and show menu
            }
        }

    }
    
    public void Resume() // function called Resume 
    {
        pauseMenuUI.SetActive(false); // turns the pause menu canvas off
        Time.timeScale = 1f; // unfreezes the game
        GameIsPaused = false; // sets bool back to false
    }

    void Pause() // function called Pause
    {
        pauseMenuUI.SetActive(true); // this will display the pause menu canvas
        Time.timeScale = 0f; // freezes the game
        GameIsPaused = true; // sets the bool to true
    }

    public void QuitGame() // function called quit game
    {
        Debug.Log("Quitting Game..."); // send message to player in inspector that the game ended
        Application.Quit();// will quit the game when built
    }
}
