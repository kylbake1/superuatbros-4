﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class goomba : MonoBehaviour {
    public GameObject player; // allows user to place player into inspector
	// Use this for initialization
	void Start () {
        player = GameObject.FindWithTag("Player"); // player is now attached to a tag called player
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	// when player enters trigger add points to gameManager and play sound
	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.gameObject.tag == "Bullet") // if the goomba is triggered with a object with tag bullet
		{
			
			Destroy(this.gameObject); // goomba is destroyed
            player.SendMessage("ScorePoints", GameManager.instance.enemyGoombaWorthPoints); // points are awarded to player in gamemanager
            Destroy(collision.gameObject);// destroys the bullet
            
		}
	}
}

   