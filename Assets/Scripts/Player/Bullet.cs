﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

	private Vector3 direction;

	// Use this for initialization
	void Start () {

		direction = GameManager.instance.player.transform.right; // bullet will move in relation to player direction
		Destroy(this.gameObject, GameManager.instance.fireBallSpeed); // bullet is destroyed after user desired set time limit
        
    }

	// Update is called once per frame
	void Update () {

		transform.Translate(direction * Time.deltaTime * GameManager.instance.fireBallSpeed); // bullets will move every frame
	}
}
