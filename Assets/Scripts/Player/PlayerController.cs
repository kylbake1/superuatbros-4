﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    public static GameManager instance; // Allows us to link to the Game manager 

    Transform tf; // stores Transform in string tf;


    public int playerSpeed;  //  creates a public int that the user can adjust for player speed
    public int playerJumpPower; //  creates a public int that the user can adjust for jumping power
    public int enemyBouncePower; //  creates a public int that the user can adjust for goomba
    public float distanceToBottomOfPlayer = 0.9f;// this is the raycast distance
   
    private float moveInput; //  creates a private float that the user cant adjust int the inspector

    public bool isGrounded; // public bool so we can use it for making sure the player is grounded
    public Transform groundCheck; // check to make sure player is grounded
    public float checkRadius;// this will go under players feet to act as a trigger
    public LayerMask whatIsGround; // this will attach to ground in layer

    private int extraJump; // private in so we can set the number of jumps to 1
    public int extraJumpValue; //  this will allow the user to add one more jump or more without changing the jump value of 1
    

    public bool facingRight; // bool to help deterimine if the player is facing the correct direction
    private Rigidbody2D rb; // rigidbody called rb and its private

    public int score; // creates a public int called score that can be viewed and changed in the inspector
    public Text scoreText; // creates a public text called scoreText for the inspector and script
    public Text livesText; // creates a public text called livesText for the inspector and script

    public Transform firePoint; // this will determine where we can fire our fireballs from
    public GameObject fireBall; // this will allow us to attach the fireball prefab in the inspector

    public AudioClip[] audioClips; // public array called audoclips so we can select from the options
    AudioSource audioSource; // used the AudioSource component and calls is audioSource



    void Start()
    {
        audioSource = GetComponent<AudioSource>(); // get the component with the audiosource attached and calls it audioSource when game starts

        extraJump = extraJumpValue; //  sets out extraJump back to original plus extra value for double jumping
        rb = GetComponent<Rigidbody2D>(); // rb is now called rigidbody2d
        scoreText.text = "Score " + score; // will display the Score in the canvas UI on screen
        livesText.text = "Lives " + GameManager.instance.numLives; // will display the lives in the canvas UI on screen


    }

    // Update is called once per frame
    void Update()
    {
        
        tf = GetComponent<Transform>(); // used tf for Transform and sets it equal to compnenet Transform. 
       
        // Shoot fireball
        if (Input.GetKeyDown(KeyCode.LeftShift)) // player pushes space bar
        {
            GameObject bullet = Instantiate<GameObject>(GameManager.instance.fireBall, firePoint.position, firePoint.rotation); // bullet will clone in hierarchy
            bullet.transform.position = transform.position;// bullet has movement applied to travel across screen
            if (!audioSource.isPlaying || audioSource.isPlaying)
            {
                audioSource.clip = audioClips[1]; // this will select audio in array index nummber 1
                audioSource.Play();// will play the audio
            }
        }

        if(isGrounded == true) // bool for player is grounded and is set to true
        {
            extraJump = extraJumpValue; // jumps are resetted and can double jump again
        }

        if(Input.GetKeyDown(KeyCode.Space)&& extraJump > 0) //  if the space bar is pressed and the extrajump value is 0
        {
            rb.velocity = Vector2.up * playerJumpPower; // player will jump by velocity 
            extraJump--; // extra jump is reduced by 1 and cant jump again
        }else if(Input.GetKeyDown(KeyCode.Space) && extraJump == 0 && isGrounded == true) // if player hasnt jumped or landed
        {
            rb.velocity = Vector2.up * playerJumpPower; // player will jump 
        }

        
    }
    void FixedUpdate()
    {
        float xMovement = Input.GetAxis("Horizontal") * playerSpeed; // this will move our player horizontally by user set speed and call it xMovement for x axis

        rb.velocity = new Vector2(xMovement,rb.velocity.y); // this will apply a rigidbody to the player and move

        if (rb.velocity.y > 0.1f) // if the movement in the vertical or y direction is greater than .1
        {
           
            GetComponent<SpriteRenderer>().flipX = false;// dont flip the player
            if (!audioSource.isPlaying) // if there is no other audio playing
            {
                audioSource.clip = audioClips[2]; // select audio 2 in array
                audioSource.Play(); // and play the audio
            }

        }
        else if (rb.velocity.x > 0.1f) // if the movement in the horizontal or x direction is greater than .1
        {
            GetComponent<Animator>().SetBool("isRunning", true); //activate the animation for running
            GetComponent<SpriteRenderer>().flipX = false; // dont flip the axis when looking right already
            if (!audioSource.isPlaying)// if there is no other audio playing
            {
                audioSource.clip = audioClips[0];// select audio 0 in array
                audioSource.Play();// and play the audio
            }
        }
        else if (rb.velocity.x < -0.1f)// if the movement in the horizontal or x direction is less than .1
        {
            GetComponent<Animator>().SetBool("isRunning", true); //activate the animation for running
            GetComponent<SpriteRenderer>().flipX = true;// flip the player in opposite direction, left
            if (!audioSource.isPlaying)// if there is no other audio playing
            {
                audioSource.clip = audioClips[0];// select audio 0 in array
                audioSource.Play();// and play the audio
            }
        }
        else // if the player stops moving or jumping
        {
            GetComponent<Animator>().SetBool("isRunning", false); // stop the animaton for running
            audioSource.Stop();// stop the audio
        }
        //CheckPointManager.instance.UpdateCheckPoint();

        isGrounded = Physics2D.OverlapCircle(groundCheck.position, checkRadius, whatIsGround); // this will check if the user is touching ground by using ground check, the trigger radius, and layer attached to ground
    }

    void OnTriggerEnter2D(Collider2D other) // trigger that will happen when touched
    {
        if(other.gameObject.tag == "enemy") // if player touches a object with tag enemy
        {
            LifeLost(); // they lose a life 
            if (!audioSource.isPlaying || audioSource.isPlaying) // if there is audio playing or if there isnt
            {
                audioSource.clip = audioClips[4]; // select audio index 4
                audioSource.Play(); // play the audio
            }

        }
        if(other.gameObject.tag == "OutOfBounds") // if player touches a object with tag outofbounds
        {
            LifeLost(); // they lose a life
            if (!audioSource.isPlaying || audioSource.isPlaying)// if there is audio playing or if there isnt
            {
                audioSource.clip = audioClips[4]; // select audio index 4
                audioSource.Play();// play the audio
            }
        }
    }

    void LifeLost()
    {
        GameManager.instance.numLives--; // lives are reduced by 1
        livesText.text = "Lives: " + GameManager.instance.numLives; // displays the new number of lives
        if(GameManager.instance.numLives == 0) // if lives is equal to 0
        {
            Debug.Log("Game Over, No More Lives"); // display message in console
            GameOver(); // the game is over
            SceneManager.LoadScene(5); // load scene index 5 with game over

        }
        else // if now
        {
            tf.transform.position = new Vector3(0, 0, 0); // Change to last location respawn or checkpoint
        }
    }

    void GameOver()
    {
        //gameOverPanel.SetActive(true);
    }

    public void PlayAgain()
    {
        tf.transform.position = new Vector3(0, 0, 0); // restart the player back at player beginning
        // Load game over again from first scene
    }

    void ScorePoints(int pointsToAdd) // function with int pointstoadd for coints and goomba
    {
        score += pointsToAdd; // current score is added to new points and  makes new score
        scoreText.text = "Score: " + score; // displays the new current score in UI
    }
}
