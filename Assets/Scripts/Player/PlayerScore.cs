﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerScore : MonoBehaviour
{
    public float timeLeft; //  creates a public float that the user can adjust for time left on screen
    public int scorePlayer;//  creates a public int that the user can adjust for the score 
    public GameObject timeLeftUI;//  creates a public GameObject that the user can attach the timeLeftUi in the inspector

    void Start()
    {
        
    }

    void Update()
    {
        timeLeft -= Time.deltaTime; // this will deduct time from time left when we set the value for it
        timeLeftUI.gameObject.GetComponent<Text>().text = ("Time: " + (int)timeLeft); //  this wil display the remaining time left in the canvas UI
        if (timeLeft < 0.1f)
        {
            SceneManager.LoadScene("SampleScene"); //  if the time is .1f than the scene will reload the 1 index by name
        }
    }

    void OnTriggerEnter2D(Collider2D trig)
    {
        if (trig.gameObject.name == "EndLevel"){ //  if the player reaches the end of the level with the name endlevel
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1); // the scene will load the next scene by 1 index number
        }
        if (trig.gameObject.tag == "Coin") // if the coin with coin tag is triggered
        {
            GetComponent<AudioSource>().Play(); // coin will play audio 
            GameManager.instance.player.SendMessage("ScorePoints", GameManager.instance.coinPoints); // and points will be added to the scorepoints from the gamemanager
            
            Destroy(trig.gameObject); // coin is then destroyed
           
        }
    }
}
