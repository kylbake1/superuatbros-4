﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

    public void PlayGame()
    {
        SceneManager.LoadScene(1); // this will load the scene with index number 1
        
    }

    public void QuitGame()
    {
        Debug.Log("QUIT");// messgae for user in console 
        Application.Quit();// will quit the game when built
    }
}

}
