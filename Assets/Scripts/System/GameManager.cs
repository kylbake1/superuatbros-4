﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    //Singleton
    public static GameManager instance;

    // SuperUatBros
    public GameObject fireBall;// this allows us to insert the fireball prefab in the inspector
    public float fireBallSpeed; //  creates a public float that the user can adjust for fire ball speed
    public float fireBallLife; //  creates a public float that the user can adjust for fire ball life duration
    public float enemyGoombaSpeed; //  creates a public float that the user can adjust for goomba speed
    public float enemyGoombaWorthPoints; //  creates a public float that the user can adjust for goomba points
    public GameObject player; // This is where we place the player object in Hierarchy
    public int numLives; // //  creates a public float that the user can adjust for number of lives
    public int coinPoints;//  creates a public float that the user can adjust for point coint for coins
    //public int numLivesValue;
    public int score;//  creates a public float that the user can adjust so we can set score



    //Checkpoints



    void Awake() // This will make sure not to destroy the Game Manager when moving to a different scene
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        Lives();
        //CheckPointManager.istance.LoadCheckPoint();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Reset()
    {
        //CheckPointManager.instance.Reset();
    }

    public void Lives()
    {
        numLives = 3; // number of lives is preset to 3
    }

    public void QuitGame() // this function will initiate the quit game process
    {
        Debug.Log("Exiting Game");
        Application.Quit();
    }

}
