﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clouds : MonoBehaviour
{
    public float speed; //  creates a public float that the user can adjust called speed for cloud
    private float xDir; //  creates a public float that the user can adjust for cloud

    // Start is called before the first frame update
    void Start()
    {
        xDir = transform.position.x; // variable called xDir that will move the clouds horizontal position
    }

    // Update is called once per frame
    void Update()
    {
        xDir -= Time.deltaTime * speed; // move the cloud in real time
        transform.position = new Vector3(xDir, transform.position.y, transform.position.z); // change position of cloud to move it across screen
    }
}
